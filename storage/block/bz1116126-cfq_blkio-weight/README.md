# storage/block/bz1116126-cfq_blkio-weight
Storage: bz1116126-cfq_blkio-weight

## How to run it
Please refer to the top-level README.md for common dependencies.

### Install dependencies
```bash
root# bash ../../../cki_bin/pkgs_install.sh metadata
```

### Execute the test
```bash
bash ./runtest.sh
```
